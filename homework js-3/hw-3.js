function countNumber(num1, num2, oper) {
    if (oper === "+") {
        return num1 + num2;
    }
    if (oper === "-") {
        return num1 - num2;
    }
    if (oper === "/") {
        return num1 / num2;
    }
    if (oper === "*") {
        return num1 * num2;
    }
}
let x = +prompt("Введите число 1");
let y = +prompt("Введите число 2");
let operation = prompt('Ваше действие');

console.log(countNumber(x, y, operation));
